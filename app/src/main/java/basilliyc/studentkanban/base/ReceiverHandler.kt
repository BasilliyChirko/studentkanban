package basilliyc.studentkanban.base

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import basilliyc.extensions.createBroadcastReceiver

interface ReceiverHandlerInterface {
    fun addReceiver(intentFilter: IntentFilter, receiver: BroadcastReceiver)

    fun addReceiver(action: String, receiver: BroadcastReceiver)

    fun addReceiver(action: List<String>, receiver: BroadcastReceiver)

    fun addReceiver(intentFilter: IntentFilter, call: (intent: Intent) -> Unit)

    fun addReceiver(action: String, call: (intent: Intent) -> Unit)

    fun addReceiver(action: List<String>, call: (intent: Intent) -> Unit)

}

class ReceiverHandler(private var context: Context) {

    private data class Receiver(
            val broadcast: BroadcastReceiver,
            val filter: IntentFilter
    )

    private val data = ArrayList<Receiver>()

    fun addReceiver(intentFilter: IntentFilter, receiver: BroadcastReceiver) {
        data.add(Receiver(receiver, intentFilter))
    }

    fun addReceiver(action: String, receiver: BroadcastReceiver) {
        val filter = IntentFilter()
        filter.addAction(action)
        addReceiver(filter, receiver)
    }

    fun addReceiver(action: List<String>, receiver: BroadcastReceiver) {
        val filter = IntentFilter()
        action.forEach { filter.addAction(it) }
        addReceiver(filter, receiver)
    }

    fun addReceiver(intentFilter: IntentFilter, call: (intent: Intent) -> Unit) {
        val receiver = createBroadcastReceiver(call)
        addReceiver(intentFilter, receiver)
    }

    fun addReceiver(action: String, call: (intent: Intent) -> Unit) {
        val receiver = createBroadcastReceiver(call)
        val filter = IntentFilter()
        filter.addAction(action)
        addReceiver(filter, receiver)
    }

    fun addReceiver(action: List<String>, call: (intent: Intent) -> Unit) {
        val receiver = createBroadcastReceiver(call)
        val filter = IntentFilter()
        action.forEach { filter.addAction(it) }
        addReceiver(filter, receiver)
    }

    fun register() {
        data.forEach {
            context.registerReceiver(it.broadcast, it.filter)
        }
    }

    fun unregister() {
        data.forEach {
            context.unregisterReceiver(it.broadcast)
        }
    }

}