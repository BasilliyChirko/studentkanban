package basilliyc.studentkanban.base

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.realm.Realm

open class BaseFragment: Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val contentView = setContentView()
        if (contentView != 0) {
            return inflater.inflate(contentView, container, false)
        }
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    open fun setContentView() : Int {
        return 0
    }


}