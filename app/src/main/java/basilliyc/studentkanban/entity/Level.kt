package basilliyc.studentkanban.entity

import basilliyc.studentkanban.module.profile.DepositPoint
import basilliyc.studentkanban.module.profile.SkilPoint

data class Level(
        var name: String = "",
        var value: Int = 0
)

fun getAllLevels() : ArrayList<Level> {
    return arrayListOf(
            Level("A", 16),
            Level("B", 8),
            Level("C", 4),
            Level("D", 2),
            Level("E", 1)
    )
}

val levelA: Level get() = Level("A", 16)
val levelB: Level get() = Level("B", 8)
val levelC: Level get() = Level("C", 4)
val levelD: Level get() = Level("D", 2)
val levelE: Level get() = Level("E", 1)


data class Task(
        var level: Level,
        var title: String,
        var description: String,
        var worker: Worker,
        var type: String
)

data class Worker(
        var id: Long = 0,
        var name: String,
        var photo: String,
        var skils: ArrayList<SkilPoint> = ArrayList(),
        var deposit: ArrayList<DepositPoint> = ArrayList()
)