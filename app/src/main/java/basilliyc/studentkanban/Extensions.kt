package basilliyc.studentkanban

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v4.app.FragmentActivity
import android.support.v4.app.FragmentManager
import android.support.v7.app.AlertDialog
import android.view.View
import java.util.concurrent.TimeUnit

class LoadingDialog : DialogFragment() {
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = AlertDialog.Builder(activity!!)
                .setView(activity!!.layoutInflater.inflate(R.layout.self_loading, null, false))
                .create()
        dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCancelable(false)
        dialog.setCanceledOnTouchOutside(false)
        return dialog
    }

    fun start(manager: FragmentManager): LoadingDialog {
        show(manager, "loading")
        return this
    }

    companion object {
        fun start(manager: FragmentManager): LoadingDialog {
            return LoadingDialog().start(manager)
        }
    }
}

fun FragmentActivity.createLoadingDialog(): LoadingDialog {
    return LoadingDialog.start(this.supportFragmentManager)
}


fun String.getNumbers(): String {
    val builder = StringBuilder()

    this.forEach {
        when (it) {
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' -> builder.append(it)
        }
    }

    return builder.toString()
}

fun Double.formatRating(): String {
    if (this < 0) return "0.0"
    return String.format("%.1f", this).replace(',', '.')
}

fun Long.yearsToCurrentMoment(): Long {
    val days = TimeUnit.MILLISECONDS.toDays(System.currentTimeMillis() - this)
    return days / 365
}

fun String.toUpperCaseFirstOnWord(): String {
    val b = StringBuilder()
    val split = this.split(" ")
    split.forEachIndexed { index, it ->
        b.append(it.first().toUpperCase())
        b.append(it.substring(1).toLowerCase())
        if (index != split.lastIndex)
            b.append(' ')
    }
    return b.toString()
}


fun getStatusBarHeight(view: View): Int {
    var result = 0
    val resourceId = view.context.resources.getIdentifier("status_bar_height", "dimen", "android")

    if (resourceId > 0) {
        result = view.context.resources.getDimensionPixelSize(resourceId)
    }
    return result
}