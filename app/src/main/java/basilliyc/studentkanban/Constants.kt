package basilliyc.studentkanban

import java.util.regex.Pattern

object Constants {
    const val DATABASE_VERSION = 1L

    const val BASE_URL = "http://www.beauty-ua.com/api/"
    const val logServerResponse = true

    val phonePattern = Pattern.compile("\\+38\\(0[0-9]{2}\\)[0-9]{3}-[0-9]{2}-[0-9]{2}")!!
    val emailPattern = Pattern.compile(".+@.+\\.+.+")!!
    val namePattern = Pattern.compile("[a-zA-Zа-яА-Я\\-\\s]+")!!

    const val birthdayResponseFormat = "yyyy-MM-dd HH:mm:ss"

    val codeRegExp = Regex("[0-9]{5}")

    object Recovery {
        const val methodEmail = "email"
        const val methodPhone = "phone"
    }
}