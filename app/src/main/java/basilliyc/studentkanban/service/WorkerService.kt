package basilliyc.studentkanban.service

import basilliyc.studentkanban.entity.*
import basilliyc.studentkanban.module.profile.DepositPoint
import basilliyc.studentkanban.module.profile.SkilPoint

object WorkerService {
    fun getAllWorkers(): ArrayList<Worker> {
        val android = Worker(1, "Василь Чирко", "https://www.atlassian.com/dam/jcr:13a574c1-390b-4bfb-956b-6b6d114bf98c/max-rehkopf.png")

        android.skils = arrayListOf(
                SkilPoint("Android", "B"),
                SkilPoint("Back-end", "D"),
                SkilPoint("Front-end", "E")
        )

        android.deposit = arrayListOf(
                DepositPoint(levelA, 1),
                DepositPoint(levelC, 1),
                DepositPoint(levelE, 4)
        )

        val backend = Worker(2, "Микола", "https://www.submitexpress.com/wp-content/uploads/2016/08/ANONYMOUS_USER.png")

        backend.skils = arrayListOf(
                SkilPoint("Back-end", "B"),
                SkilPoint("Front-end", "D")
        )

        backend.deposit = arrayListOf(
                DepositPoint(levelA, 1),
                DepositPoint(levelC, 3),
                DepositPoint(levelE, 3)
        )

        val design = Worker(3, "Катерина", "http://www.stickpng.com/assets/images/585e4bc4cb11b227491c3395.png")

        design.skils = arrayListOf(
                SkilPoint("Design", "B")
        )

        design.deposit = arrayListOf(
                DepositPoint(levelB, 2),
                DepositPoint(levelC, 3),
                DepositPoint(levelE, 1)
        )

        return arrayListOf(android, backend, design)
    }


    fun getOne(id:Long): Worker? {
        return getAllWorkers().find { it.id == id }
    }

}