@file:Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")

package basilliyc.studentkanban

import basilliyc.extensions.load
import basilliyc.extensions.save

object Preferences {
    /**

    object Preferences {
    //EXAMPLE OF USAGE
    var foo
    get() = load(object {}.javaClass.enclosingMethod, 0f)
    set(value) = value.save(object {}.javaClass.enclosingMethod)
    }
     */


    var currentUserId
        get() = load("currentUserId", 0L)
        set(value) = value.save("currentUserId")


}