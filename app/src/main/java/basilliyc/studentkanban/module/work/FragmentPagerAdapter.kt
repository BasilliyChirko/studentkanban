package basilliyc.studentkanban.module.work

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter

class FragmentPagerAdapter(val data: ArrayList<Fragment>, fragmentManager: FragmentManager) : FragmentStatePagerAdapter(fragmentManager) {
    override fun getItem(p0: Int): Fragment {
        return data[p0]
    }

    override fun getCount(): Int = data.size
}