package basilliyc.studentkanban.module.work

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import basilliyc.studentkanban.R
import basilliyc.studentkanban.entity.*
import kotlinx.android.synthetic.main.activity_work.*

class WorkActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_work)
        title = "Список завдань"

        val android = Worker(1,"Василь Чирко", "https://www.atlassian.com/dam/jcr:13a574c1-390b-4bfb-956b-6b6d114bf98c/max-rehkopf.png")
        val backend = Worker(2, "Микола", "https://www.submitexpress.com/wp-content/uploads/2016/08/ANONYMOUS_USER.png")
        val design = Worker(3, "Катерина", "http://www.stickpng.com/assets/images/585e4bc4cb11b227491c3395.png")

        val taskToDo = arrayListOf(
                Task(levelB, "Ендпойнт музики в чаті", "", backend, "Back-end"),
                Task(levelC, "Макети згрупованих фото в чаті", "", design, "Design"),
                Task(levelC, "Верстка згрупованих фото в чаті", "", android, "Android"),
                Task(levelB, "Підв'язка музики в чаті", "", android, "Android"),
                Task(levelE, "Верстка файлу в чаті", "", android, "Android"),
                Task(levelC, "Підв'язка файлу в чаті", "", android, "Android"),
                Task(levelC, "Ендпойнт файлу в чаті", "", backend, "Back-end")
        )

        val taskInWork = arrayListOf(
                Task(levelB, "Верстка музики в чаті", "", android, "Android"),
                Task(levelB, "Ендпойнт отримання згрупованих фото", "", backend, "Back-end"),
                Task(levelE, "Макети файлу в чаті", "", design, "Design")
        )

        val taskTest = arrayListOf(
                Task(levelC, "Підв'язка загального чату", "", android, "Android")
        )
        val taskDone = arrayListOf(
                Task(levelD, "Макети загального чату", "", design, "Design"),
                Task(levelD, "Верстка загального чату", "", android, "Android"),
                Task(levelC, "Ендпойнти загального чату", "", backend, "Back-end"),
                Task(levelD, "Підв'язка діалогу", "", android, "Android"),
                Task(levelD, "Верстка діалогу", "", android, "Android"),
                Task(levelD, "Ендпойнти діалогу", "", backend, "Back-end"),
                Task(levelD, "Макети діалогу", "", design, "Design")
        )

        pager.adapter = FragmentPagerAdapter(arrayListOf(
                WorkFragment("До виконання", taskToDo),
                WorkFragment("В роботі", taskInWork, 3),
                WorkFragment("Тестується", taskTest, 2),
                WorkFragment("Готово", taskDone)
        ), supportFragmentManager)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.account, menu)
        return true
    }
}