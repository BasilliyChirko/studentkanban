package basilliyc.studentkanban.module.work

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import basilliyc.studentkanban.R
import basilliyc.studentkanban.base.BaseFragment
import basilliyc.studentkanban.entity.Task
import kotlinx.android.synthetic.main.fragment_work_page.*

@SuppressLint("ValidFragment")
class WorkFragment(val title: String, val data: ArrayList<Task>, val maxCount: Int = -1) : BaseFragment() {
    override fun setContentView(): Int = R.layout.fragment_work_page

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        title_fragment.text = (if (maxCount == -1) title else "$title ${data.size}/$maxCount")

        work_list.layoutManager = LinearLayoutManager(activity)
        work_list.itemAnimator = DefaultItemAnimator()
        work_list.adapter = TaskRecyclerAdadpter(data)
    }

}