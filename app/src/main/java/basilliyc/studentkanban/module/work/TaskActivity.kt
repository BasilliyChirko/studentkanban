package basilliyc.studentkanban.module.work

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import basilliyc.studentkanban.R

class TaskActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_task)
        title = "Пререгляд завдання"
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.edit, menu)
        return true
    }
}