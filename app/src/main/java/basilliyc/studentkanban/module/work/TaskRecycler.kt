package basilliyc.studentkanban.module.work

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import basilliyc.extensions.inflate
import basilliyc.studentkanban.R
import basilliyc.studentkanban.entity.Task
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_task.view.*

class TaskRecyclerHolder(val view: View) : RecyclerView.ViewHolder(view) {
    fun onBind(item: Task) {
        view.title.text = item.title
        view.level.text = item.level.name
        view.type.text = item.type
        Picasso.get().load(item.worker.photo).into(view.photo)
    }
}

class TaskRecyclerAdadpter(val data: ArrayList<Task>) : RecyclerView.Adapter<TaskRecyclerHolder>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): TaskRecyclerHolder {
        return TaskRecyclerHolder(p0.inflate(R.layout.item_task))
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(p0: TaskRecyclerHolder, p1: Int) {
        p0.onBind(data[p1])
    }
}