package basilliyc.studentkanban.module.team

import basilliyc.extensions.BaseRecyclerAdapter
import basilliyc.extensions.BaseRecyclerHolder
import basilliyc.studentkanban.R
import basilliyc.studentkanban.entity.Worker
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_team.view.*

class TeamRecyclerAdapter : BaseRecyclerAdapter<Worker>() {
    override val layout: Int
        get() = R.layout.item_team

    override fun onBindHolder(holder: BaseRecyclerHolder<Worker>, item: Worker) {
        with(holder.view) {
            Picasso.get().load(item.photo).into(team_image)
            team_name.text = item.name
        }
    }
}