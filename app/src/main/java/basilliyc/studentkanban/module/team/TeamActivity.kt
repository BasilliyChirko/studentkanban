package basilliyc.studentkanban.module.team

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import basilliyc.studentkanban.R
import basilliyc.studentkanban.module.profile.ProfileActivity
import basilliyc.studentkanban.service.WorkerService
import kotlinx.android.synthetic.main.activity_team.*

class TeamActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_team)

        title = "Список учасників"

        team_list.layoutManager = LinearLayoutManager(this)
        team_list.itemAnimator = DefaultItemAnimator()
        team_list.adapter = TeamRecyclerAdapter().setData(WorkerService.getAllWorkers()).onClick {
            val intent = Intent(this, ProfileActivity::class.java)
            intent.putExtra(ProfileActivity.KEY_ID, it.id)
            startActivity(intent)
        }
    }
}