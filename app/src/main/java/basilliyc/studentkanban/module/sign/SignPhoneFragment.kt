package basilliyc.studentkanban.module.sign

import android.os.Bundle
import android.view.View
import basilliyc.studentkanban.R
import basilliyc.studentkanban.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_sign_phone.*

class SignPhoneFragment : BaseFragment() {

    var onSubmit: (() -> Unit)? = null

    override fun setContentView(): Int = R.layout.fragment_sign_phone

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sign_submit.setOnClickListener {
            onSubmit?.invoke()
        }
    }

}