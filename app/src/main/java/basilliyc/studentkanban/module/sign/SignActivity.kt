package basilliyc.studentkanban.module.sign

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import basilliyc.extensions.toast
import basilliyc.studentkanban.R
import basilliyc.studentkanban.module.sign.SignActivity.Page.Code
import basilliyc.studentkanban.module.sign.SignActivity.Page.Phone

class SignActivity : AppCompatActivity() {

    enum class Page {
        Phone, Code
    }

    //Page Fragments
    private var fragmentPhone = SignPhoneFragment()
    private var fragmentCode = SignCodeFragment()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign)
        initFragmentListeners()
        setPage(Phone, false)
    }

    private fun setPage(page: Page, backStack: Boolean = true) {
        val fragment = when (page) {
            Phone -> {
                fragmentPhone
            }
            Code -> {
                fragmentCode
            }
        }

        val transaction = supportFragmentManager
                .beginTransaction()
                .setCustomAnimations(
                        android.R.animator.fade_in,
                        android.R.animator.fade_out,
                        android.R.animator.fade_in,
                        android.R.animator.fade_out
                ).replace(R.id.sign_up_container, fragment)

        if (backStack)
            transaction.addToBackStack(null)

        transaction.commit()
    }

    private fun initFragmentListeners() {
        fragmentPhone.onSubmit = {
            setPage(Code)
        }

        fragmentCode.onSubmit = {
            toast("ok")
        }

    }
}