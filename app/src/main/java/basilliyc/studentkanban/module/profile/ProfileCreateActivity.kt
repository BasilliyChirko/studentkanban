package basilliyc.studentkanban.module.profile

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import basilliyc.studentkanban.R
import kotlinx.android.synthetic.main.activity_profile_create.*

class ProfileCreateActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile_create)
//        title = "Створення профілю"
        title = "Редагування профілю"

        list.layoutManager = LinearLayoutManager(this)
        list.itemAnimator = DefaultItemAnimator()
        list.adapter = SkillRecyclerAdapter(arrayListOf(
                SkilPoint("Android", "B"),
                SkilPoint("Back-end", "D"),
                SkilPoint("Front-end", "E")
        ))
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.done, menu)
        return true
    }
}