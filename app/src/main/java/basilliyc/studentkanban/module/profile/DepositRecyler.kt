package basilliyc.studentkanban.module.profile

import android.annotation.SuppressLint
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import basilliyc.extensions.inflate
import basilliyc.studentkanban.R
import basilliyc.studentkanban.entity.Level
import kotlinx.android.synthetic.main.item_profile_skils.view.*

data class DepositPoint(
        var level: Level,
        var count: Int = 0
)

class DepositHolder(val view: View) : RecyclerView.ViewHolder(view) {
    @SuppressLint("SetTextI18n")
    fun onBind(item: DepositPoint) {
        view.title.text = "${item.level.name} - ${item.count} (${item.count * item.level.value})"
        view.clear.visibility = View.GONE
    }
}

class DepositRecyclerAdapter(var data: ArrayList<DepositPoint>) : RecyclerView.Adapter<DepositHolder>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): DepositHolder {
        return DepositHolder(p0.inflate(R.layout.item_profile_skils))
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(p0: DepositHolder, p1: Int) {
        p0.onBind(data[p1])
    }

}