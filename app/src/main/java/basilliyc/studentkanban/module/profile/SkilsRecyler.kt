package basilliyc.studentkanban.module.profile

import android.annotation.SuppressLint
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import basilliyc.extensions.inflate
import basilliyc.studentkanban.R
import kotlinx.android.synthetic.main.item_profile_skils.view.*

data class SkilPoint (
        var name: String = "",
        var level: String = ""
)

class SkilHolder(val view: View, val canDelete: Boolean = true) : RecyclerView.ViewHolder(view) {
    @SuppressLint("SetTextI18n")
    fun onBind(item: SkilPoint) {
        view.title.text = "${item.name} - ${item.level}"
        view.clear.visibility = if (canDelete) View.VISIBLE else View.GONE
    }
}

class SkillRecyclerAdapter(var data: ArrayList<SkilPoint>, private val canDelete: Boolean = true) : RecyclerView.Adapter<SkilHolder>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): SkilHolder {
        return SkilHolder(p0.inflate(R.layout.item_profile_skils), canDelete)
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(p0: SkilHolder, p1: Int) {
        p0.onBind(data[p1])
    }

}