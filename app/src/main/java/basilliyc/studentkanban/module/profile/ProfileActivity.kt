package basilliyc.studentkanban.module.profile

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import basilliyc.studentkanban.R
import basilliyc.studentkanban.entity.levelA
import basilliyc.studentkanban.entity.levelC
import basilliyc.studentkanban.entity.levelE
import basilliyc.studentkanban.service.WorkerService
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_profile.*

class ProfileActivity : AppCompatActivity() {

    companion object {
        const val KEY_ID = "KEY_ID"
    }

    var isCurrent = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        if (intent != null && intent.getLongExtra(KEY_ID, -1L) != -1L) {
            val worker = WorkerService.getOne(intent.getLongExtra(KEY_ID, -1L))!!
            isCurrent = false

            title = "Перегляд профилю"

            Picasso.get().load(worker.photo).into(circleImageView)
            editText.text = worker.name

            list.layoutManager = LinearLayoutManager(this)
            list.itemAnimator = DefaultItemAnimator()
            list.adapter = SkillRecyclerAdapter(worker.skils, false)

            list_project.layoutManager = LinearLayoutManager(this)
            list_project.itemAnimator = DefaultItemAnimator()
            list_project.adapter = DepositRecyclerAdapter(worker.deposit)

        } else {
            isCurrent = true
            title = " профилю"

            list.layoutManager = LinearLayoutManager(this)
            list.itemAnimator = DefaultItemAnimator()
            list.adapter = SkillRecyclerAdapter(arrayListOf(
                    SkilPoint("Android", "B"),
                    SkilPoint("Back-end", "D"),
                    SkilPoint("Front-end", "E")
            ), false)

            list_project.layoutManager = LinearLayoutManager(this)
            list_project.itemAnimator = DefaultItemAnimator()
            list_project.adapter = DepositRecyclerAdapter(arrayListOf(
                    DepositPoint(levelA, 1),
                    DepositPoint(levelC, 1),
                    DepositPoint(levelE, 4)
            ))
        }


    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        return if (isCurrent) {
            val inflater = menuInflater
            inflater.inflate(R.menu.edit, menu)
            true
        } else false
    }
}