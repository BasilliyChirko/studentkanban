package basilliyc.extensions

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup

abstract class BaseRecyclerHolder<T>(val view: View) : RecyclerView.ViewHolder(view) {
    abstract fun onBind(item: T)
}

abstract class BaseRecyclerAdapter<T>(val data: ArrayList<T> = ArrayList(), var onClick: ((T) -> Unit)? = null, var onClickView: ((T, View) -> Unit)? = null) : RecyclerView.Adapter<BaseRecyclerHolder<T>>() {

    abstract val layout: Int

    abstract fun onBindHolder(holder: BaseRecyclerHolder<T>, item: T)

    fun setData(list: List<T>): BaseRecyclerAdapter<T> {
        data.clear()
        data.addAll(list)
        notifyDataSetChanged()
        return this
    }

    fun onClick(call:(T) -> Unit): BaseRecyclerAdapter<T> {
        onClick = call
        return this
    }

    fun onClickView(call:(T, View) -> Unit): BaseRecyclerAdapter<T> {
        onClickView = call
        return this
    }

    override fun onCreateViewHolder(parent: ViewGroup, type: Int): BaseRecyclerHolder<T> {
        return object : BaseRecyclerHolder<T>(parent.inflate(layout)) {
            override fun onBind(item: T) {
                onBindHolder(this, item)
            }
        }
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: BaseRecyclerHolder<T>, position: Int) {
        val item = data[position]
        holder.onBind(item)
        if (onClick != null) {
            holder.view.setOnClickListener {
                onClick?.invoke(item)
            }
        } else if (onClickView != null) {
            holder.view.setOnClickListener {
                onClickView?.invoke(item, holder.view)
            }
        }
    }
}