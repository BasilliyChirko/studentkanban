package basilliyc.extensions

import android.util.DisplayMetrics
import android.util.TypedValue

object MetricsExtensions {
    lateinit var instance: DisplayMetrics

    fun init(metrics: DisplayMetrics) {
        instance = metrics
    }
}

/** METRICS */

fun Float.toPx(): Float = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, this, MetricsExtensions.instance)

fun Double.toPx(): Float = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, this.toFloat(), MetricsExtensions.instance)

fun Int.toPx(): Float = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, this.toFloat(), MetricsExtensions.instance)

fun Long.toPx(): Float = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, this.toFloat(), MetricsExtensions.instance)