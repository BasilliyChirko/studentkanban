@file:Suppress("IMPLICIT_CAST_TO_ANY", "UNCHECKED_CAST")

package basilliyc.extensions

import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.os.AsyncTask
import kotlin.reflect.KClass
import kotlin.reflect.KMutableProperty
import kotlin.reflect.KProperty1
import kotlin.reflect.full.createInstance
import kotlin.reflect.full.declaredMemberProperties
import kotlin.reflect.full.findAnnotation

fun <T : Any> Context.database(clazz: KClass<T>): SQLTable<T> {
    return SQLTable(clazz, this)
}


fun KMutableProperty<*>.isClass(): Boolean {
    val toString = returnType.classifier.toString()
    return !toString.startsWith("class kotlin.") && !toString.startsWith("class java.util.ArrayList")
}

fun KMutableProperty<*>.isPrimitive(): Boolean {
    return returnType.classifier.toString().startsWith("class kotlin.")
}

fun KMutableProperty<*>.isListPrimitive(): Boolean {
    return getter.findAnnotation<Many>()?.kClass?.isPrimitive() ?: false
}

fun KMutableProperty<*>.isListClass(): Boolean {
    return getter.findAnnotation<Many>()?.kClass?.isPrimitive() == false ?: false
}

fun KMutableProperty<*>.getKClass(): KClass<out Any> {
    return Class.forName(returnType.classifier.toString().substring(6)).kotlin
}

fun KProperty1<out Any, Any?>.isClass(): Boolean {
    val toString = returnType.classifier.toString()
    return !toString.startsWith("class kotlin.") && !toString.startsWith("class java.util.ArrayList")
}

fun KProperty1<out Any, Any?>.isPrimitive(): Boolean {
    return returnType.classifier.toString().startsWith("class kotlin.")
}

fun KProperty1<out Any, Any?>.isListPrimitive(): Boolean {
    return getter.findAnnotation<Many>()?.kClass?.isPrimitive() ?: false
}

fun KProperty1<out Any, Any?>.isListClass(): Boolean {
    return getter.findAnnotation<Many>()?.kClass?.isPrimitive() == false ?: false
}

fun KProperty1<out Any, Any?>.getKClass(): KClass<out Any> {
    return Class.forName(returnType.classifier.toString().substring(6)).kotlin
}

fun KProperty1<out Any, Any?>.sqlType(): String {
    return when (returnType.toString()) {
        "kotlin.String" -> "TEXT"
        "kotlin.Int", "kotlin.Long", "kotlin.Boolean" -> "INTEGER"
        "kotlin.Float", "kotlin.Double" -> "REAL"
        else -> ""
    }
}

fun KClass<out Any>.isPrimitive(): Boolean {
    return toString().startsWith("class kotlin.")
}

fun KClass<out Any>.sqlType(): String {
    return when (toString()) {
        "class kotlin.String" -> "TEXT"
        "class kotlin.Int", "class kotlin.Long", "class kotlin.Boolean" -> "INTEGER"
        "class kotlin.Float", "class kotlin.Double" -> "REAL"
        else -> ""
    }
}


fun StringBuilder.appendName(x: Any?): StringBuilder {
    if (x is String) append("`$x`")
    else append(x)
    return this
}

fun StringBuilder.appendParam(x: Any?): StringBuilder {
    if (x is String) append("'$x'")
    else append(x)
    return this
}

fun StringBuilder.space(): StringBuilder {
    append(" ")
    return this
}


fun KClass<out Any>.buildCreateStatementPrimitive(parentClass: KClass<out Any>, field: KProperty1<out Any, Any?>, parentId: KProperty1<out Any, Any?>): String {
    return "CREATE TABLE `${parentClass.createPrimitiveSubTableName(field)}` (`id` ${parentId.sqlType()}, `value` ${sqlType()});"
}

fun KClass<out Any>.buildDropStatementPrimitive(parentClass: KClass<out Any>, field: KProperty1<out Any, Any?>): String {
    return "DROP TABLE `${parentClass.createPrimitiveSubTableName(field)}`;"
}

fun KClass<out Any>.buildInsertStatementPrimitive(parentClass: KClass<out Any>, field: KProperty1<out Any, Any?>, parentId: Any, value: List<Any>): String {
    with(StringBuilder()) {
        append("INSERT INTO `${parentClass.createPrimitiveSubTableName(field)}` (`id`,`value`) VALUES ")

        value.forEachIndexed { index, any ->
            append("(")
            appendParam(parentId)
            append(",")
            appendParam(any)
            append(")")
            if (index != value.lastIndex) append(", ")
        }

        append(";")
        return toString()
    }
}

fun KClass<out Any>.buildDeleteStatementPrimitive(parentClass: KClass<out Any>, field: KProperty1<out Any, Any?>, parentId: Any): String {
    with(StringBuilder()) {
        append("DELETE FROM `${parentClass.createPrimitiveSubTableName(field)}` WHERE `id`=")
        appendParam(parentId)
        append(";")
        return toString()
    }
}

fun KClass<out Any>.buildClearStatementPrimitive(parentClass: KClass<out Any>, field: KProperty1<out Any, Any?>): String {
    return "DELETE FROM `${parentClass.createPrimitiveSubTableName(field)}`;"
}

fun KClass<out Any>.buildSelectStatementPrimitive(parentClass: KClass<out Any>, field: KProperty1<out Any, Any?>, id: Any?): String {
    with(StringBuilder()) {
        append("SELECT `value` FROM `${parentClass.createPrimitiveSubTableName(field)}` WHERE `id`=")
        appendParam(id)
        append(";")
        return toString()
    }
}

fun KClass<out Any>.createPrimitiveSubTableName(field: KProperty1<out Any, Any?>): String {
    return simpleName + "." + field.name
}


fun KClass<out Any>.buildCreateStatementBridge(kClass: KClass<out Any>, field: KProperty1<out Any, Any?>, primaryKey: KProperty1<out Any, Any?>): String {
    return "CREATE TABLE `${createBridgeTableName(field)}` (`parent` ${primaryKey.sqlType()}, `child` ${kClass.primaryKey.sqlType()});"
}

fun KClass<out Any>.buildDropStatementBridge(field: KProperty1<out Any, Any?>): String {
    return "DROP TABLE `${createBridgeTableName(field)}`;"
}

fun KClass<out Any>.buildDeleteStatementBridge(kClass: KClass<out Any>, field: KProperty1<out Any, Any?>, parentId: Any): List<String> {
    return arrayListOf(
            with(StringBuilder()) {
                append("DELETE FROM ${kClass.tableName} WHERE `${kClass.primaryKey.name}` in (SELECT `child` FROM `${createBridgeTableName(field)}` WHERE `parent`=")
                appendParam(parentId)
                append(");")
                toString()
            },
            with(StringBuilder()) {
                append("DELETE FROM `${createBridgeTableName(field)}` WHERE `parent`=")
                appendParam(parentId)
                append(";")
                toString()
            }
    )
}

fun KClass<out Any>.buildClearStatementBridge(kClass: KClass<out Any>, field: KProperty1<out Any, Any?>): List<String> {
    return arrayListOf(
            "DELETE FROM ${kClass.tableName} WHERE `${kClass.primaryKey.name}` in (SELECT `child` FROM `${createBridgeTableName(field)}`);",
            "DELETE FROM `${createBridgeTableName(field)}`;"
    )
}

fun KClass<out Any>.buildInsertStatementBridge(field: KProperty1<out Any, Any?>, parentId: Any, value: List<Any>): List<String> {
    return arrayListOf(
            with(StringBuilder()) {
                append("DELETE FROM `${createBridgeTableName(field)}` WHERE `parent`=")
                appendParam(parentId)
                append(";")
                toString()
            },
            with(StringBuilder()) {
                append("INSERT INTO `${createBridgeTableName(field)}` (`parent`,`child`) VALUES ")

                value.forEachIndexed { index, any ->
                    append("(")
                    appendParam(parentId)
                    append(",")
                    appendParam(any)
                    append(")")
                    if (index != value.lastIndex) append(", ")
                }

                append(";")
                toString()
            }

    )
}

fun KClass<out Any>.buildSelectStatementBridge(field: KProperty1<out Any, Any?>, parentId: Any): String {
    with(StringBuilder()) {
        append("SELECT `child` FROM `${createBridgeTableName(field)}` WHERE `parent`=")
        appendParam(parentId)
        append(";")
        return toString()
    }
}

fun KClass<out Any>.createBridgeTableName(field: KProperty1<out Any, Any?>): String {
    return simpleName + "." + field.name
}


fun KClass<out Any>.buildCreateStatement(): List<String> {
    val list = ArrayList<String>()
    with(StringBuilder()) {
        append("CREATE TABLE $tableName (")

        val primaryKey = primaryKey

        properties.forEachIndexed { index, field ->
            when {
                field.isListClass() -> {
                    val kClass = field.getter.findAnnotation<Many>()!!.kClass
                    list.addAll(kClass.buildCreateStatement())
                    list.add(this@buildCreateStatement.buildCreateStatementBridge(kClass, field, primaryKey))
                    return@forEachIndexed
                }
                field.isListPrimitive() -> {
                    val kClass = field.getter.findAnnotation<Many>()!!.kClass
                    list.add(kClass.buildCreateStatementPrimitive(this@buildCreateStatement, field, primaryKey))
                    return@forEachIndexed
                }
                field.isClass() -> {
                    appendName(field.name)
                    append(" ${field.getKClass().primaryKey.sqlType()}")
                }
                else -> {
                    appendName(field.name)
                    append(" ${field.sqlType()}")
                    field.getter.findAnnotation<PrimaryKey>()?.let {
                        append(" PRIMARY KEY")
                        if (it.autoIncrement && field.sqlType() == "INTEGER") append(" AUTOINCREMENT")
                    }
                }
            }

            if (index != properties.size - 1)
                append(",").space()
        }

        append(");")

        list.add(toString())
    }

    classProperties.tryForEach {
        list.addAll(it.getKClass().buildCreateStatement())
    }

    return list
}

fun KClass<out Any>.buildDropStatement(): List<String> {
    val list = ArrayList<String>()
    list.add("DROP TABLE $tableName;")
    classProperties.tryForEach {
        list.addAll(it.getKClass().buildDropStatement())
    }
    listPrimitiveProperties.tryForEach {
        list.add(it.getter.findAnnotation<Many>()!!.kClass.buildDropStatementPrimitive(this, it))
    }

    listClassProperties.tryForEach {
        val kClass = it.getter.findAnnotation<Many>()!!.kClass
        list.addAll(kClass.buildDropStatement())
        list.add(buildDropStatementBridge(it))
    }
    return list
}

fun KClass<out Any>.buildInsertStatement(value: Any): List<String> {
    val list = ArrayList<String>()
    with(StringBuilder()) {
        append("INSERT OR REPLACE INTO $tableName")

        val properties = properties.filter { prop ->
            val getter = prop.getter
            getter.findAnnotation<Many>()?.let {
                return@filter false
            }
            getter.findAnnotation<PrimaryKey>()?.let {
                return@filter !(it.autoIncrement && (getter.call(value) == 0 || getter.call(value) == 0L))
            }
            true
        }

        properties.forEachIndexed { index, field ->
            if (index == 0) append(" (")
            appendName(field.name)

            if (index == properties.size - 1) append(")")
            else append(", ")
        }

        properties.forEachIndexed { index, field ->
            if (index == 0) append(" VALUES (")

            when {
                field.isClass() -> {
                    val classValue = field.getter.call(value)
                    if (classValue == null) append("NULL")
                    else appendParam(field.getKClass().primaryKey.getter.call(classValue))
                }
                field.returnType.toString() == "kotlin.Boolean" -> appendParam(if (field.getter.call(value) == true) 1 else 0)
                else -> appendParam(field.getter.call(value))
            }

            if (index == properties.size - 1) append(")")
            else append(", ")
        }
        append(";")
        list.add(toString())
    }
    classProperties.tryForEach {
        val param = it.getter.call(value)
        if (param != null)
            list.addAll(it.getKClass().buildInsertStatement(param))
    }

    val primaryValue = primaryKey.getter.call(value)!!

    listPrimitiveProperties.tryForEach {
        val kClass = it.getter.findAnnotation<Many>()!!.kClass
        list.add(kClass.buildDeleteStatementPrimitive(this, it, primaryValue))
        list.add(kClass.buildInsertStatementPrimitive(this, it, primaryValue, it.getter.call(value) as List<Any>))
    }

    listClassProperties.tryForEach {
        val kClass = it.getter.findAnnotation<Many>()!!.kClass
        val subData = it.getter.call(value) as List<Any>
        subData.forEach {
            list.addAll(kClass.buildInsertStatement(it))
        }

        val subGetter = kClass.primaryKey.getter
        val keys = subData.map { subGetter.call(it)!! }
        list.addAll(buildInsertStatementBridge(it, primaryValue, keys))
    }

    return list
}

fun KClass<out Any>.buildDeleteStatement(value: Any): List<String> {
    val list = ArrayList<String>()
    val primaryKey = primaryKey
    val primaryValue = primaryKey.call(value)!!
    with(StringBuilder()) {
        append("DELETE FROM $tableName WHERE ")
        appendName(primaryKey.name)
        append("=")
        appendParam(primaryValue)
        append(";")
        list.add(toString())
    }

    classProperties.tryForEach {
        val param = it.getter.call(value)
        if (param != null)
            list.addAll(it.getKClass().buildDeleteStatement(param))
    }

    listPrimitiveProperties.tryForEach {
        list.add(it.getter.findAnnotation<Many>()!!.kClass.buildDeleteStatementPrimitive(this, it, primaryValue))
    }

    listClassProperties.tryForEach {
        val kClass = it.getter.findAnnotation<Many>()!!.kClass
        list.addAll(buildDeleteStatementBridge(kClass, it, primaryValue))
    }
    return list
}

fun KClass<out Any>.buildSelectOneStatement(id: Any): String {
    return StringBuilder()
            .append("SELECT * FROM $tableName WHERE ")
            .appendName(primaryKey.name)
            .append("=")
            .appendParam(id)
            .append(";")
            .toString()
}

fun KClass<out Any>.buildSelectAllStatement(): String {
    return "SELECT * FROM $tableName"
}

fun KClass<out Any>.buildClearStatement(): List<String> {
    val list = ArrayList<String>()
    list.add("DELETE FROM $tableName;")
    classProperties.tryForEach {
        list.addAll(it.getKClass().buildClearStatement())
    }
    listPrimitiveProperties.tryForEach {
        list.add(it.getter.findAnnotation<Many>()!!.kClass.buildClearStatementPrimitive(this, it))
    }
    listClassProperties.tryForEach {
        val kClass = it.getter.findAnnotation<Many>()!!.kClass
        list.addAll(buildClearStatementBridge(kClass, it))
    }
    return list
}

fun KClass<out Any>.getItem(id: Any, db: SQLiteDatabase): Any? {
    val cursor = db.raw(buildSelectOneStatement(id))

    if (cursor.count == 0) {
        return null
    }

    var item: Any? = null
    val parentClass = this
    try {
        cursor.moveToNext()
        with(createInstance()) {
            properties.filterIsInstance<KMutableProperty<*>>().forEach {
                when {
                    it.isListClass() -> {
                        val kClass = it.getter.findAnnotation<Many>()!!.kClass
                        val query = parentClass.buildSelectStatementBridge(it as KProperty1<out Any, Any?>, id)
                        val listCursor = db.raw(query)
                        val list = ArrayList<Any?>()
                        while (listCursor.moveToNext()) {
                            val paramPrimitive = listCursor.paramClassId(kClass)
                            val element = kClass.getItem(paramPrimitive!!, db)
                            list.add(element)
                        }
                        listCursor.close()
                        it.setter.call(this, list)
                    }
                    it.isListPrimitive() -> {
                        val kClass = it.getter.findAnnotation<Many>()!!.kClass
                        val listCursor = db.raw(kClass.buildSelectStatementPrimitive(this::class, it as KProperty1<out Any, Any?>, id))
                        val list = ArrayList<Any?>()
                        while (listCursor.moveToNext()) {
                            list.add(listCursor.paramPrimitive(kClass))
                        }
                        listCursor.close()
                        it.setter.call(this, list)
                    }
                    it.isClass() -> {
                        val param = cursor.param(it)
                        if (param != null) {
                            val args = it.getKClass().getItem(param, db)
                            it.setter.call(this, args)
                        }
                    }
                    else -> it.setter.call(this, cursor.param(it))
                }
            }
            item = this
        }
    } catch (e: Exception) {
        error("getItem $e")
    } finally {
        cursor.close()
    }

    return item
}


fun SQLiteDatabase.exe(query: String) {
    if (SQLTable.debug) {
        log(query)
        SQLTable.executionCount++
    }
    execSQL(query)
}

fun SQLiteDatabase.raw(query: String): Cursor {
    if (SQLTable.debug) {
        log(query)
        SQLTable.executionCount++
    }
    return rawQuery(query, arrayOf())
}

inline fun <T> Collection<T>.tryForEach(call: (T) -> Unit) {
    forEach {
        try {
            call(it)
        } catch (e: Exception) {
        }
    }
}


val KClass<out Any>.tableName: String
    get() = "`${this.simpleName}`"

val KClass<out Any>.properties: List<KProperty1<out Any, Any?>>
    get() = declaredMemberProperties.filter {
        it.getter.findAnnotation<Ignore>()?.let { return@filter false }
        true
    }

val KClass<out Any>.primitiveProperties: List<KProperty1<out Any, Any?>>
    get() = properties.filter { it.isPrimitive() }

val KClass<out Any>.classProperties: List<KProperty1<out Any, Any?>>
    get() = properties.filter { it.isClass() }

val KClass<out Any>.listPrimitiveProperties: List<KProperty1<out Any, Any?>>
    get() = properties.filter { it.isListPrimitive() }

val KClass<out Any>.listClassProperties: List<KProperty1<out Any, Any?>>
    get() = properties.filter { it.isListClass() }

val KClass<out Any>.primaryKey: KProperty1<out Any, Any?>
    get() = properties.firstOrNull { it.getter.findAnnotation<PrimaryKey>() != null }!!

fun Cursor.param(prop: KMutableProperty<*>): Any? {
    val any = when (prop.returnType.toString()) {
        "kotlin.String" -> getString(getColumnIndex(prop.name))
        "kotlin.Int" -> getInt(getColumnIndex(prop.name))
        "kotlin.Long" -> getLong(getColumnIndex(prop.name))
        "kotlin.Float" -> getFloat(getColumnIndex(prop.name))
        "kotlin.Double" -> getDouble(getColumnIndex(prop.name))
        "kotlin.Boolean" -> getInt(getColumnIndex(prop.name)) == 1
        else -> null
    }

    if (any == null && prop.isClass()) {
        return when (prop.getKClass().primaryKey.returnType.classifier.toString().substring("class ".length)) {
            "kotlin.String" -> getString(getColumnIndex(prop.name))
            "kotlin.Int" -> getInt(getColumnIndex(prop.name))
            "kotlin.Long" -> getLong(getColumnIndex(prop.name))
            "kotlin.Float" -> getFloat(getColumnIndex(prop.name))
            "kotlin.Double" -> getDouble(getColumnIndex(prop.name))
            else -> null
        }
    }
    return any
}

fun Cursor.paramPrimitive(prop: KClass<out Any>): Any? {
    return when (prop.toString()) {
        "class kotlin.String" -> getString(0)
        "class kotlin.Int" -> getInt(0)
        "class kotlin.Long" -> getLong(0)
        "class kotlin.Float" -> getFloat(0)
        "class kotlin.Double" -> getDouble(0)
        "class kotlin.Boolean" -> getInt(0) == 1
        else -> null
    }
}

fun Cursor.paramClassId(prop: KClass<out Any>): Any? {
    return when (prop.primaryKey.returnType.toString()) {
        "kotlin.String" -> getString(0)
        "kotlin.Int" -> getInt(0)
        "kotlin.Long" -> getLong(0)
        "kotlin.Float" -> getFloat(0)
        "kotlin.Double" -> getDouble(0)
        "kotlin.Boolean" -> getInt(0) == 1
        else -> null
    }
}

interface SimpleDatabase<T> {
    fun save(value: T)

    fun get(id: Any): T?

    fun all(): ArrayList<T>

    fun clear()

    fun remove(value: Any)
}

open class SQLTable<T : Any>(val x: KClass<out T>, context: Context) {

    open fun createScheme(context: Context): SQLScheme {
        return SQLScheme(context, SQLScheme.configuration.databaseName, SQLScheme.configuration.version)
    }

    private val scheme by lazy { createScheme(context) }

    private val readableDatabase = scheme.readableDatabase

    private val writableDatabase = scheme.writableDatabase

    private val properties: Collection<KProperty1<out T, Any?>> by lazy {
        x.declaredMemberProperties.filter {
            it.getter.findAnnotation<Ignore>()?.let { return@filter false }
            true
        }
    }

    open fun save(value: T) = transaction { db ->
        x.buildInsertStatement(value).tryForEach { db.exe(it) }
    }

    open fun get(id: Any): T? {
        return x.getItem(id, readableDatabase) as T?
    }

    open fun all(): ArrayList<T> {
        val list = ArrayList<T>()

        val cursor = readableDatabase.raw(x.buildSelectAllStatement())
        try {
            while (cursor.moveToNext()) with(x.createInstance()) {
                x.primitiveProperties.filterIsInstance<KMutableProperty<*>>().forEach {
                    val args = cursor.param(it)
                    it.setter.call(this, args)
                }

                val id = x.primaryKey.getter.call(this)

                x.listPrimitiveProperties.filterIsInstance<KMutableProperty<*>>().forEach {
                    val kClass = it.getter.findAnnotation<Many>()!!.kClass
                    val listCursor = readableDatabase.raw(kClass.buildSelectStatementPrimitive(this::class, it as KProperty1<out Any, Any?>, id))
                    val data = ArrayList<Any?>()
                    while (listCursor.moveToNext()) {
                        data.add(listCursor.paramPrimitive(kClass))
                    }
                    listCursor.close()
                    it.setter.call(this, data)
                }

                x.listClassProperties.filterIsInstance<KMutableProperty<*>>().forEach {
                    val kClass = it.getter.findAnnotation<Many>()!!.kClass
                    val query = this::class.buildSelectStatementBridge(it as KProperty1<out Any, Any?>, id!!)
                    val listCursor = readableDatabase.raw(query)
                    val data = ArrayList<Any?>()
                    while (listCursor.moveToNext()) {
                        val paramPrimitive = listCursor.paramClassId(kClass)
                        val element = kClass.getItem(paramPrimitive!!, readableDatabase)
                        data.add(element)
                    }
                    listCursor.close()
                    it.setter.call(this, data)
                }

                x.classProperties.filterIsInstance<KMutableProperty<*>>().forEach {
                    val param = cursor.param(it)
                    if (param != null) {
                        val args = it.getKClass().getItem(param, readableDatabase)
                        it.setter.call(this, args)
                    }
                }

                list.add(this)
            }
        } catch (e: Exception) {
            error(e)
        } finally {
            cursor.close()
        }

        return list
    }

    open fun clear() = transaction { db ->
        x.buildClearStatement().tryForEach { db.exe(it) }
    }

    open fun remove(value: Any) = transaction { db ->
        x.buildDeleteStatement(value).tryForEach {
            db.exe(it)
        }
    }

    private inline fun transaction(call: (SQLiteDatabase) -> Unit) {
        val database = writableDatabase
        try {
            database.beginTransaction()
            call(database)
            database.setTransactionSuccessful()
        } catch (e: Exception) {
            e.printStackTrace()
            kotlin.error(e)
        } finally {
            database.endTransaction()
        }
    }

    fun async(call: (SQLTable<T>) -> Unit) {
        val asyncTask = AsyncRun<T>()
        asyncTask.call = call
        asyncTask.table = this
        asyncTask.execute()
    }

    private class AsyncRun<T : Any> : AsyncTask<Int, Int, Int>() {

        lateinit var call: (SQLTable<T>) -> Unit
        lateinit var table: SQLTable<T>

        override fun doInBackground(vararg params: Int?): Int {
            call(table)
            return 0
        }
    }

    fun close() {
        scheme.close()
    }

    companion object {
        val debug = false
        var executionCount = 0L
    }

}

data class SQLConfiguration(
        var databaseName: String = "service",
        var version: Int = 1,
        var tables: ArrayList<KClass<out Any>> = ArrayList()
)

open class SQLScheme(val context: Context, name: String, version: Int) : SQLiteOpenHelper(context, name, null, version) {

    companion object {
        var configuration = SQLConfiguration()
    }

    override fun onCreate(db: SQLiteDatabase) {
        configuration.tables
                .flatMap { it.buildCreateStatement() }
                .toSet()
                .tryForEach { db.exe(it) }
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        configuration.tables
                .flatMap { it.buildDropStatement() }
                .toSet()
                .tryForEach { db.exe(it) }
        onCreate(db)
    }
}

@Target(AnnotationTarget.VALUE_PARAMETER, AnnotationTarget.PROPERTY_GETTER, AnnotationTarget.PROPERTY, AnnotationTarget.FIELD)
annotation class PrimaryKey(val autoIncrement: Boolean = true)

@Target(AnnotationTarget.VALUE_PARAMETER, AnnotationTarget.PROPERTY_GETTER, AnnotationTarget.PROPERTY, AnnotationTarget.FIELD)
annotation class Ignore

@Target(AnnotationTarget.VALUE_PARAMETER, AnnotationTarget.PROPERTY_GETTER, AnnotationTarget.PROPERTY, AnnotationTarget.FIELD)
annotation class Many(val kClass: KClass<out Any>)

@Target(AnnotationTarget.CLASS)
annotation class Table

