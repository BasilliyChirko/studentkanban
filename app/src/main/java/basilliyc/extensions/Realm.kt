package basilliyc.extensions

import com.vicpin.krealmextensions.*
import io.realm.RealmObject

abstract class BaseDatabaseOne<T : RealmObject>(private val clazz: T) {
    var value: T?
        get() = clazz.queryFirst()
        set(value) = value!!.save()
}

abstract class BaseDatabaseList<T : RealmObject>(private val clazz: T) {

    fun add(item: T) {
        item.save()
    }

//    fun add(items: List<T>) {
//        items.saveAll()
//    }

    fun get(id: Long): T? {
        return clazz.queryFirst { equalTo("id", id) }
    }

    fun all(): List<T> {
        return clazz.queryAll()
    }

    fun clear() {
        clazz.deleteAll()
    }

    fun isEmpty(): Boolean = clazz.count() == 0L

    fun isNotEmpty(): Boolean = !isEmpty()
}