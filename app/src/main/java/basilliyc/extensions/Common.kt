@file:Suppress("DEPRECATION")

package basilliyc.extensions

import android.annotation.SuppressLint
import android.app.Activity
import android.app.DatePickerDialog
import android.app.Fragment
import android.app.TimePickerDialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.graphics.Typeface
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import java.math.BigInteger
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.text.SimpleDateFormat
import java.util.*

/** LOGGER */

val TAG = "AndroidRuntime"

fun Context.toast(x: Any?, length: Int = Toast.LENGTH_SHORT) {
    when (x) {
        null -> Toast.makeText(this, "null", length).show()
        is Int -> Toast.makeText(this, x, length).show()
        else -> Toast.makeText(this, x.toString(), length).show()
    }
}

fun android.support.v4.app.Fragment.toast(x: Any?, length: Int = Toast.LENGTH_SHORT) {
    activity?.toast(x, length)
}

fun Any.log(x: Any? = this) {
    when (x) {
        null -> Log.i(TAG, "null")
        else -> Log.i(TAG, x.toString())
    }
}

fun Any.error(x: Any? = this) {
    when (x) {
        null -> Log.e(TAG, "null")
        else -> Log.e(TAG, x.toString())
    }
}

fun Any.debug(x: Any? = this) {
    when (x) {
        null -> Log.d(TAG, "null")
        else -> Log.d(TAG, x.toString())
    }
}

fun <T> Collection<T>.logAll() {
    this.forEach {
        log(it)
    }
}


/** ACTIVITY */

fun <T> Activity.goToActivity(clazz: Class<T>, finish: Boolean = false) {
    start(clazz)
    if (finish)
        finish()
}

fun <T> Activity.goToActivity(clazz: Class<T>, request: Int) {
    startActivityForResult(Intent(this, clazz), request)
}

fun <T> Fragment.goToActivity(clazz: Class<T>) {
    activity?.start(clazz)
}

fun <T> Context.start(clazz: Class<T>) {
    startActivity(Intent(this, clazz))
}


/** TIME */

fun Long.toCalendar(): Calendar {
    val calendar = Calendar.getInstance()
    calendar.timeInMillis = this
    return calendar
}

const val minInMillis = 1000L * 60L
const val hourInMillis = 1000L * 60L * 60L
const val dayInMillis = 1000L * 60L * 60L * 24L
const val monthInMillis = 1000L * 60L * 60L * 24L * 30L
const val yearInMillis = 1000L * 60L * 60L * 24L * 30L * 12L


/** STRING */

@SuppressLint("SimpleDateFormat")
fun Long.formatTime(pattern: String): String {
    return SimpleDateFormat(pattern).format(Date(this))
}

@SuppressLint("SimpleDateFormat")
fun String.formatTime(pattern: String): Long {
    return SimpleDateFormat(pattern).parse(this).time
}


/** SERVICE */

fun createBroadcastReceiver(call: (intent: Intent) -> Unit): BroadcastReceiver {
    return object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            intent?.let(call)
        }
    }
}


/** ANIMATION */

fun View.animateVisibility(show: Boolean, duration: Long = 300, hideType: Int = View.GONE) {
    visibility = if (show) View.VISIBLE else hideType
    alpha = if (show) 0f else 1f
    animate()
            .alpha(if (show) 1f else 0f)
            .setDuration(duration)
            .setInterpolator(AccelerateDecelerateInterpolator())
            .start()
}

fun View.visible(show: Boolean, hideType: Int = View.GONE) {
    visibility = if (show) View.VISIBLE else hideType
}

/** EDIT TEXT */
fun EditText.validate(error: String, haveError: (String) -> Boolean): Boolean {
    val isError = haveError(this.text.toString().trim())
    if (isError) this.error = error
    return !isError
}

fun EditText.validate(error: Int, haveError: (String) -> Boolean): Boolean {
    val isError = haveError(this.text.toString().trim())
    if (isError) this.error = context.resources.getString(error)
    return !isError
}

fun EditText.validateNoTrim(error: Int, haveError: (String) -> Boolean): Boolean {
    val isError = haveError(this.text.toString())
    if (isError) this.error = context.resources.getString(error)
    return !isError
}

fun EditText.textValue(trim: Boolean = false): String {
    return if (trim) text.toString().trim() else text.toString()
}

//fun EditText.validate(haveError: (String) -> Boolean): Boolean {
//    return !haveError(this.text.toString().trim())
//}

inline fun EditText.onAction(crossinline callback: () -> Unit) {
    setOnEditorActionListener { _, i, _ ->
        if (i == EditorInfo.IME_ACTION_DONE ||
                i == EditorInfo.IME_ACTION_NEXT ||
                i == EditorInfo.IME_ACTION_GO ||
                i == EditorInfo.IME_ACTION_SEARCH ||
                i == EditorInfo.IME_ACTION_SEND) {
            callback()
            return@setOnEditorActionListener true
        }
        false
    }
}

inline fun TextView.afterChange(crossinline callback: (s: String) -> Unit) {
    addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(s: Editable) {
            callback(s.toString())
        }

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
    })
}

inline fun TextView.beforeChange(crossinline callback: (s: String) -> Unit) {
    addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(s: Editable) {}

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            callback(s.toString())
        }

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
    })
}

inline fun TextView.onChange(crossinline callback: (s: String) -> Unit) {
    addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(s: Editable) {}

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            callback(s.toString())
        }
    })
}


fun Any.md5(): String {
    val st = this.toString()
    var digest = ByteArray(0)

    try {
        val messageDigest = MessageDigest.getInstance("MD5")
        messageDigest.reset()
        messageDigest.update(st.toByteArray())
        digest = messageDigest.digest()
    } catch (ignore: NoSuchAlgorithmException) {
    }

    var md5Hex = BigInteger(1, digest).toString(16)

    while (md5Hex.length < 32) {
        md5Hex = "0$md5Hex"
    }

    return md5Hex
}

fun ViewGroup.inflate(layoutId: Int, attachToRoot: Boolean = false): View {
    return LayoutInflater.from(this.context).inflate(layoutId, this, attachToRoot)
}


fun showTimePicker(context: Context, calendar: Calendar, callback: (calendar: Calendar) -> Unit) {
    TimePickerDialog(
            context,
            TimePickerDialog.OnTimeSetListener { _, h, m ->
                val cal = GregorianCalendar()
                cal.timeInMillis = calendar.timeInMillis
                cal.set(Calendar.HOUR_OF_DAY, h)
                cal.set(Calendar.MINUTE, m)
                callback(cal)
            },
            calendar.get(Calendar.HOUR_OF_DAY),
            calendar.get(Calendar.MINUTE),
            true
    ).show()
}

fun showDatePicker(context: Context, theme: Int = 0, calendar: Calendar = Calendar.getInstance(), minDate: Long = 0L, callback: (calendar: Calendar) -> Unit) {
    val datePickerDialog = if (theme == 0) DatePickerDialog(
            context,
            DatePickerDialog.OnDateSetListener { _, y, m, d ->
                val cal = GregorianCalendar()
                cal.timeInMillis = calendar.timeInMillis
                cal.set(y, m, d)
                callback(cal)
            },
            calendar.get(Calendar.YEAR),
            calendar.get(Calendar.MONTH),
            calendar.get(Calendar.DAY_OF_MONTH)
    ) else DatePickerDialog(
            context,
            theme,
            DatePickerDialog.OnDateSetListener { _, y, m, d ->
                val cal = GregorianCalendar()
                cal.timeInMillis = calendar.timeInMillis
                cal.set(y, m, d)
                callback(cal)
            },
            calendar.get(Calendar.YEAR),
            calendar.get(Calendar.MONTH),
            calendar.get(Calendar.DAY_OF_MONTH)
    )

    if (minDate != 0L) {
        datePickerDialog.datePicker.minDate = minDate
    }

    datePickerDialog.show()
}


fun Context.getColorValue(resId: Int): Int {
    return resources.getColor(resId)
}

fun View.getColorValue(resId: Int): Int {
    return resources.getColor(resId)
}

fun View.getDimen(resource: Int): Int {
    return context.resources.getDimensionPixelSize(resource)
}

fun Context.createTypeface(path: String): Typeface {
    return Typeface.createFromAsset(assets, path)
}