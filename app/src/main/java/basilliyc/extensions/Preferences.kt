@file:Suppress("UNCHECKED_CAST")

package basilliyc.extensions

import android.content.Context
import android.content.SharedPreferences
import basilliyc.extensions.PreferencesExtensions.pref
import java.lang.reflect.Method
import kotlin.reflect.KClass
import kotlin.reflect.KMutableProperty
import kotlin.reflect.full.createInstance
import kotlin.reflect.full.findAnnotation

/**

object Preferences {
//EXAMPLE OF USAGE
var foo
get() = load(object {}.javaClass.enclosingMethod, 0f)
set(value) = value.save(object {}.javaClass.enclosingMethod)
}
 */


private fun String.parseName(): String {
    if (this.startsWith("get") || this.startsWith("set")) {
        return this.substring(3)
    }
    return this
}

object PreferencesExtensions {
    lateinit var pref: SharedPreferences

    fun init(context: Context) {
        pref = context.getSharedPreferences("app.preferences", Context.MODE_PRIVATE)
    }

}

fun Any.save(method: Method) {
    val key = method.name.parseName()
    val edit = pref.edit()
    when (this) {
        is Int -> edit.putInt(key, this)
        is Long -> edit.putLong(key, this)
        is Float -> edit.putFloat(key, this)
        is Boolean -> edit.putBoolean(key, this)
        is String -> edit.putString(key, this)
    }
    edit.apply()
}

fun <T> load(method: Method, default: T): T {
    val key = method.name.parseName()
    return when (default) {
        is Int -> pref.getInt(key, default as Int) as T
        is Long -> pref.getLong(key, default as Long) as T
        is Float -> pref.getFloat(key, default as Float) as T
        is Boolean -> pref.getBoolean(key, default as Boolean) as T
        is String -> pref.getString(key, default as String) as T
        else -> "" as T
    }
}

fun Any.save(key: String) {
    val edit = pref.edit()
    when (this) {
        is Int -> edit.putInt(key, this)
        is Long -> edit.putLong(key, this)
        is Float -> edit.putFloat(key, this)
        is Boolean -> edit.putBoolean(key, this)
        is String -> edit.putString(key, this)
    }
    edit.apply()
}

fun <T> load(key: String, default: T): T {
    return when (default) {
        is Int -> pref.getInt(key, default as Int) as T
        is Long -> pref.getLong(key, default as Long) as T
        is Float -> pref.getFloat(key, default as Float) as T
        is Boolean -> pref.getBoolean(key, default as Boolean) as T
        is String -> pref.getString(key, default as String) as T
        else -> "" as T
    }
}


